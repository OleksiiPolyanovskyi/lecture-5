import { v4 as uuidv4 } from 'uuid'

export function createPost(text, currentUser){
  return {
    id: uuidv4(),
    userId: currentUser.id,    
    user: currentUser.name,
    text: text,
    createdAt: new Date().toISOString(),
    editedAt: '',
    likesCount: new Set()
  }
}

export function removePost(id, posts){
  return posts.filter(post => post.id !== id)
}

export function updatePost(post, posts){
  const copy = [...posts]
  const index = copy.findIndex(item => item.id === post.id)
  copy.splice(index, 1, post)
  return copy
}

export function likePost(post, posts, userId){  
  const copy = [...posts]
  const index = copy.findIndex(item => item.id === post.id)
  const updated = copy[index]
  updated.likesCount?.add(userId)
  copy.splice(index, 1, updated) 
  return copy 
}