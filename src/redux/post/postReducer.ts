import { 
    FETCH_POSTS_START,
     FETCH_POSTS_END, 
     SET_POSTS,
     ADD_POST,
     DELETE_POST,
     UPDATE_POST,
     LIKE_POST,
     SET_POST_FOR_UPDATE,
     REM_POST_FOR_UPDATE
} from './post.action.types'
import { likePost, removePost, updatePost } from '../../helpers/postsOperations'
import { Post } from '../../interfaces/interfaces'
import { PostActionTypes } from './post.actions'

const initialState = {
    posts:[] as Post[],
    loading:true,
    postForUpdate:null as Post | null
}

type InitialState = typeof initialState

export default (state=initialState, action:PostActionTypes):InitialState => {
    switch(action.type) {
        case FETCH_POSTS_START:
        return {
            ...state,
            loading:true
        }
        case FETCH_POSTS_END:
        return {
            ...state,
            loading:false
        }
        case SET_POSTS:
        return {
            ...state,
            posts:action.payload
        }
        case ADD_POST:
        return {
            ...state,
            posts:[action.payload, ...state.posts]
        }
        case DELETE_POST:
        return {
            ...state,
            posts: removePost(action.payload, state.posts)
        }
        case UPDATE_POST:
        return {
            ...state,
            posts: updatePost(action.payload, state.posts),
            postForUpdate:null
        }
        case LIKE_POST:
        return {
            ...state,
            posts: likePost(action.payload.post, state.posts, action.payload.userId)
        }
        case SET_POST_FOR_UPDATE:
        return {
            ...state,
            postForUpdate:action.payload            
        }
        case REM_POST_FOR_UPDATE:
        return {
            ...state,
            postForUpdate:null
        }
        default:
        return state
    }
}