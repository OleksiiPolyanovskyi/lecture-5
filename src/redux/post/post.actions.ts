import { 
    FETCH_POSTS_START, 
    FETCH_POSTS_END, 
    SET_POSTS,
    ADD_POST,
    DELETE_POST,
    UPDATE_POST,
    LIKE_POST,
    SET_POST_FOR_UPDATE,
    REM_POST_FOR_UPDATE
} from './post.action.types'
import { getPostsFromServer } from '../../helpers/callWebApi'
import { addLikeCountProp, sortPosts } from '../../helpers/propsTransformers'
import { Post } from '../../interfaces/interfaces'
import { AppStateType } from '../store'
import { ThunkAction } from 'redux-thunk'

type ThunkType = ThunkAction<Promise<void>, AppStateType, unknown, PostActionTypes>
 
type fetchPostsStartType = {
    type: typeof FETCH_POSTS_START
}
const fetchPostsStart = ():fetchPostsStartType => ({type:FETCH_POSTS_START})

type fetchPostsEndType = {
    type: typeof FETCH_POSTS_END
}
const fetchPostsEnd = ():fetchPostsEndType => ({type:FETCH_POSTS_END})

type setPostsType = {
    type: typeof SET_POSTS
    payload:Post[]
}
export const setPosts = (posts:Post[]):setPostsType => ({
    type: SET_POSTS,
    payload: posts
})

export const fetchPosts = ():ThunkType => async (dispatch) => {    
    dispatch(fetchPostsStart())
    const res = await getPostsFromServer()
    const posts = await res.json()    
    const sorted = sortPosts(posts)
    const withLikesCount = addLikeCountProp(sorted)
    dispatch(setPosts(withLikesCount))
    dispatch(fetchPostsEnd())
}

type addPostType = {
    type: typeof ADD_POST
    payload:Post
}
export const addPost = (post:Post):addPostType => ({
    type:ADD_POST,
    payload:post
})

type deletePostType = {
    type: typeof DELETE_POST
    payload:string
}
export const deletePost = (id:string):deletePostType => ({
    type:DELETE_POST,
    payload:id
})

type updatePostType = {
    type:typeof UPDATE_POST
    payload:Post
}
export const updatePost = (post:Post):updatePostType => ({
    type:UPDATE_POST,
    payload:post
})

type likePostPayload = {
    post:Post
    userId:string
}
type likePostType = {
    type:typeof LIKE_POST
    payload:likePostPayload
}
export const likePost = (post:Post, userId:string):likePostType => ({
    type:LIKE_POST,
    payload:{post, userId}
})

type setPostForUpdateType = {
    type:typeof SET_POST_FOR_UPDATE
    payload:Post
}
export const setPostForUpdate = (post:Post):setPostForUpdateType =>({
    type:SET_POST_FOR_UPDATE,
    payload:post
})

type remPostForUpdateType = {
    type:typeof REM_POST_FOR_UPDATE
}
export const remPostForUpdate = ():remPostForUpdateType =>({
    type:REM_POST_FOR_UPDATE    
})

export type PostActionTypes = fetchPostsStartType | fetchPostsEndType | setPostsType | 
addPostType | deletePostType | updatePostType | likePostType | setPostForUpdateType |
remPostForUpdateType
