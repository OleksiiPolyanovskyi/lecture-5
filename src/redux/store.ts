import {
    createStore,
    applyMiddleware,
    compose,
    combineReducers
  } from 'redux'
import thunk from 'redux-thunk'
import postReducer from './post/postReducer'
import userReducer from './user/userReducer'

const middlewares = [thunk]
const composedEnhancers = compose(
    applyMiddleware(...middlewares)
)

const reducers = {
    posts: postReducer,
    user: userReducer
}

const rootReducer = combineReducers({...reducers})

type RootReducerType = typeof rootReducer
export type AppStateType = ReturnType<RootReducerType>

export const store = createStore(
    rootReducer,    
    composedEnhancers
)
