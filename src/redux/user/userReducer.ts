import { type } from 'os'
import { createCurrentUser } from '../../helpers/userActions'
import { User } from '../../interfaces/interfaces'

const initialState:InitialState = {
    currentUser:createCurrentUser()
}

type InitialState = {
    currentUser:User
}
 
export default (state = initialState, action:any):InitialState => {    
    return state    
}