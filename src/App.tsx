import React from 'react'
import { Post, User } from './interfaces/interfaces'
import { bindActionCreators } from 'redux'
import  Spinner  from './components/spinner/spinner.component'
import Home from './components/home/home.component'
import AddPost from './components/addpost/add.component'
import UpdatePost from './components/update-post/update.component'
import { createPost } from './helpers/postsOperations'
import { connect } from 'react-redux'
import { fetchPosts, addPost, setPosts, updatePost, remPostForUpdate } from './redux/post/post.actions'
import { AppStateType } from './redux/store'
import { PostActionTypes } from './redux/post/post.actions'
import './App.scss'

type Props = {
  currentUser:User | null
  loading:boolean
  fetchPosts():void
  setPosts(posts:Post[]):void
  addPost(post:Post):void  
  updatePost(post:Post):void 
  remPostForUpdate():void 
  posts:Post[]
  postForUpdate:Post | null
}

class App extends React.Component <Props>{
  constructor(props:Props){
    super(props)     
    this.addPost = this.addPost.bind(this)
    this.updatePost = this.updatePost.bind(this)   
  }
  private headRef = React.createRef<HTMLDivElement>()

  componentDidMount() {          
    this.props.fetchPosts()       
  }

 addPost(text:string){    
    const newPost:Post = createPost(text, this.props.currentUser)       
    this.props.addPost(newPost)
    this.headRef.current?.scrollIntoView()
  }
  updatePost(post:Post){         
    this.props.updatePost(post)   
  } 
 
  render() {
    return <div className="app_container">
      {this.props.loading ? 
        <Spinner /> : 
        <div className="chat_container" ref={this.headRef}>
          <Home 
            posts={this.props.posts} 
            user={this.props.currentUser} 
            update={this.updatePost}            
            />
          <AddPost addPost={this.addPost}/>
          {this.props.postForUpdate ? 
            <UpdatePost 
              post={this.props.postForUpdate} 
              updatePost={this.updatePost}
              closeModal={this.props.remPostForUpdate}
            /> 
            : null
          }
        </div>
      }    
    </div>
  }
}

const mapStateToProps = (state:AppStateType) => ({
  currentUser:state.user.currentUser,
  loading:state.posts.loading,
  posts:state.posts.posts,
  postForUpdate:state.posts.postForUpdate
})

const actions = {
  fetchPosts,
  addPost,
  setPosts,  
  updatePost,
  remPostForUpdate 
}

const mapDispatchToProps = (dispatch:any) => bindActionCreators({...actions}, dispatch)
 
export default connect(mapStateToProps, mapDispatchToProps)(App)
