import React from 'react'
import { participantsFormating, messagesFormating } from '../../helpers/propsTransformers'
import { createDateForHeader } from '../../helpers/dateFormating'
import './header.styles.scss'

type HeaderProps = {
  usersCount: number 
  messagesCount: number
  lastDate: string
}

const Header:React.FC<HeaderProps> = (
  { 
    usersCount, 
    messagesCount, 
    lastDate 
  }) => {
  const participants = participantsFormating(usersCount)
  const messages = messagesFormating(messagesCount)
  const date = createDateForHeader(lastDate)
  return (
    <div className="header_container">
      <div className="header_left-side">
          <span>My Chat</span>
          <span>{participants}</span> 
          <span>{messages}</span>
      </div>
      <div className="header_right-side">
          <span>Last message: {date}</span>  
      </div>
    </div>
  )
}

export default Header