import React, { useState } from 'react'
import { Post } from '../../interfaces/interfaces'
import { Form, Button, Icon, Segment, Label } from 'semantic-ui-react'

import './add.styles.scss'

type AddPostProps = {
  post?:Post | null  
  addPost?(text:string):void  
  updatePost?(post:Post | null):void
}

const AddPost:React.FC <AddPostProps> = ({
  post=null,  
  addPost,
  updatePost,  
}) => {
  const initialText = post ? post.text : ''
  const [text, setText] = useState(initialText);  
 
  const addPostHandler = () => {
    if (!text) {
      return
    }
    if (post) {
      const updated = {...post, text:text}                
      if(updatePost) updatePost(updated)      
    } else {
      if (addPost) addPost(text)
      setText('')      
    }
  };  

  return (
    <Segment className={post ? "editeModeFix" : undefined} >
      <Form onSubmit={addPostHandler}>
        <Form.TextArea
          name="text"
          value={text}
          placeholder="What is the news?"
          onChange={ev => setText(ev.target.value)}
        />        
        <div className="buttonsBlock">          
          {
            !post
              ? <Button floated="right" color="blue" type="submit">Post</Button>
              : (
                <Label basic size="small" as="a" floated="right" className="toolbarBtn" onClick={addPostHandler}>
                  <Icon name="save" />
                </Label>
              )
          }
        </div>
      </Form>
    </Segment>
  );
};

export default AddPost;