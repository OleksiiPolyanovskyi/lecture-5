import React from 'react'
import{ Post, User } from '../../interfaces/interfaces'
import Header from '../header/header.component'
import PostsBlock from '../posts-block/postsBlock.component'
import { getUsersQuantity } from '../../helpers/userActions'
import { getLatestDate, changeDataFormatForBlocks, simplifyDateFormat } from '../../helpers/propsTransformers'
import './home.styles.scss'

type HomeProps = {
  posts:Post[],
  user:User | null,
  update(post:Post):void
}

const Home:React.FC <HomeProps> = ({posts, user, update}) => {
  const usersCount = getUsersQuantity(posts)
  const messagesCount = posts.length
  const lastDate = getLatestDate(posts)
  const sipmlifyedPosts = simplifyDateFormat(posts)
  const postsInBlockFormat = changeDataFormatForBlocks(sipmlifyedPosts)  
   
  return (
  <div className="home_container" >
      <Header        
        usersCount={usersCount} 
        messagesCount={messagesCount} 
        lastDate={lastDate}
       />
       <div className="posts_field">
            {Object.keys(postsInBlockFormat)
              .map((date, i) => (
              <PostsBlock 
                key={i} 
                date={date} 
                posts={postsInBlockFormat[date]} 
                user={user} 
                update={update}               
              />))}
       </div>      
  </div>
 )
}

export default Home