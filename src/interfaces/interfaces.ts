export interface Post {
    id: string
    userId: string
    avatar?: string
    user: string
    text: string
    createdAt: string | Date
    editedAt: string | Date
    likesCount?: Set<any>
}

export interface User {
  id: string
  name: string
}

